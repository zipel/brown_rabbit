<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rabbit
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
<div class="container">
<div class="main-area">


<div id="searchbar" class="box">
  <div class="container-1">
      <span class="icon"><i class="fa fa-search"></i></span>
      <form action="<?php echo home_url( '/' ); ?>" method="get">
		<input id="search" name="s" type="search" placeholder="<?php _e('Search...','rabbit') ?>" class="header-search" >
	</form>
  </div>
</div>

  <div class="top-menu-bar">
    <div id="logo">
    	<a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo" width="399" height="58"></a>
    </div>
    <div class="menu-wrap navbar-header nav-collapse">
        <!--<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php //esc_html_e( 'Primary Menu', 'rabbit' ); ?></button>-->
        <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
    <!-- #site-navigation --> 
    </div>
  </div>
  
        <div class="main-image">
          <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/coffee.png" alt="bo!"/>
      	</div>
        
        <div class="image-tagline  clearfix">
            <div id="tagline" class="text-left">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/tagline.png"/>
            </div>
            <div id="social" class="text-right">
                <ul>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/rss.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/feedburner.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/gmail.png"/>
                        </a>
                    </li>
                </ul>
            </div>
      	</div>

</div>

</div>

<a class="skip-link screen-reader-text" href="#content">
<?php esc_html_e( 'Skip to content', 'rabbit' ); ?>
</a>


      
      
<header id="masthead" class="site-header" role="banner">
  <div class="site-branding">
    <?php if ( is_front_page() && is_home() ) : ?>
    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
      <?php bloginfo( 'name' ); ?>
      </a></h1>
    <?php else : ?>
    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
      <?php bloginfo( 'name' ); ?>
      </a></p>
    <?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
    <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
    <?php
			endif; ?>
  </div>
  <!-- .site-branding --> 
  
</header>
<!-- #masthead -->

<div id="content" class="site-content">
