<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rabbit
 */

?>
</div>
<!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container clearfix">
      <div class="row">
        <div class="col-md-12 site-info">
          <div class="col-md-4">
            <h2>
              About Nordic Barista Cup
            </h2>
            <span>The vision within the Nordic Barista Cup is:</span>
            <p class="light">“To create an environment in which knowledge about coffee and its sphere can be obtained”</p>
            <p>...create an environment...’
              Combined with personally absorption having the opportunity to see and experience countries, people, traditions etc. will
              always serve as a source of inspiration in our daily work.
              The organization behind the Nordic Barista Cup see it as its main purpose to be a part of creating this forum in which people
              can meet, bond and achieve further knowledge.</p>
          </div>
          
          <div class="col-md-4">
            <h2>
              NBC Flickr Stream
            </h2>
            
            <div class="row">
                <div class="flickr-column col-sm-4">
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img1.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img4.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img7.jpg"> </a>
                </div>
                <div class="flickr-column col-sm-4">
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img2.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img5.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img8.jpg"> </a>
                </div>
                <div class="flickr-column col-sm-4">
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img3.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img6.jpg"> </a>
                  <a href="#"> <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/img9.jpg"> </a>
                </div>
            </div>
          </div>
          
          <div class="col-md-4 contact">
            <h2>
              Contact
            </h2>
              <h3 class="light">Nordic Barista Cup</h3>
              <p>Amagertorv 13<br>
                1160 Copenhagen K<br>
                +45 33 12 04 28<br>
                CVR: 11427693<br>
                Email: bbrend@nordicbaristacup.com </p>
                <ul>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/rss.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/feedburner.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/gmail.png"/>
                        </a>
                    </li>
                </ul>
          </div>
        </div>
      </div>
    </div>
  <!-- .site-info --> 
</footer>
<!-- #colophon -->
</div>
<!-- #page -->

<?php wp_footer(); ?>
</body></html>