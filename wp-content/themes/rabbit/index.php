<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rabbit
 */

get_header(); ?>


<div id="primary" class="container">
<div class="content-area">
    <main id="main" class="site-main row" role="main">
      <div class="col-md-9">
          <div class="display-posts">
              <div class="text">
					<?php
                       $query = new WP_Query( array(
                       		'posts_per_page' => 3,
                       )); 
                    ?>
                    
                    <?php if ( $query->have_posts() ) : ?>
                      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    
                        <a href="<?php the_permalink(); ?>"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></a>
                        <span>Posted: </span><?php the_date('d/m-Y', '<span>', '</span>'); ?>
                        <?php the_excerpt(); ?>
                    
                      <?php endwhile; ?>
                      <?php //wp_reset_postdata(); ?>
                    
                    <?php else : ?>
                      <p>No posts to show...</p>
                    <?php endif; ?>
              </div>
          </div>
      </div>
      <div class="col-md-3">
      	<div class="grey-sidebar">
        	<div>
                <h1>NBS Shop</h1>
                <p>Your shopping cart is empty</p>
                <a href="#">Visit the shop</a>
            </div>
            <div>
                <h1>Next Event</h1>
                <h2>Nordic Barista Cup 2011</h2>
                <p>Copenhagen, Denmark<br>
                  Dates: 25th - 27th August 2011<br>
                  Theme: SENSORY</p>
                <a href="#">Sign up now</a>
            </div>
            <div>
                <h1>Scoreboard</h1>
                <p>List of winners from past years</p>
                <ul>
                  <li>2011 - ?</li>
                  <li>2010 - Sweden</li>
                  <li>2009 - Denmark</li>
                  <li>2007 - Sweden</li>
                  <li>2006 - Norway</li>
                  <li>2005 - Norway</li>
                  <li>2004 - Denmark</li>
                </ul>
            </div>
        </div>
      </div>
    </main><!-- #main -->
</div><!-- content-area -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();